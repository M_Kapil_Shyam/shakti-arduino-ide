### (This repo is still under testing) ###
## Shakti Arduino ##

This repository is used to program Shakti Boards using Arduino IDE

### Contents ###

* Shakti core files
* Libraries files
* Uploader files
* arduino realted text files

## IDE Config file ##
* platform.txt - has the definitions of CPU architecture. It has compiler commands ,process required for build and the tools required for uploading.
* boards.txt   - has the definitions of the board such as board name and parameters for uploading the sketch.
* programmers.txt - has the definition for external programmers such as OpenOCD.

