/*
  Copyright (c) 2015 Arduino LLC.  All right reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
#ifndef _WIRING_INTERRUPTS_
#define _WIRING_INTERRUPTS_

#include <stdint.h>

#define CHANGE    2
#define FALLING   3
#define RISING    4
#define DEFAULT   1
#define EXTERNAL  0

//#define digitalPinToInterrupt(P) (PLIC_INTERRUPT_7 + P) 
typedef void (*voidFuncPtr)( void ) ;
//Mode will be added later
void attachInterrupt(uint32_t intnum, voidFuncPtr callback, uint32_t mode);
void detachInterrupt(uint32_t intnum);
void interrupts(void);
void noInterrupts(void);

#endif