openocd.name=Shakti OpenOCD RAM
openocd.communication=usb
openocd.program.params.verbose=-v
openocd.program.params.quiet=-q
openocd.program.tool=openocd

openocd_flash.name=Shakti OpenOCD Flash
openocd_flash.communication=usb
openocd_flash.program.params.verbose=-v
openocd_flash.program.params.quiet=-q
openocd_flash.program.tool=openocd_flash

